# DevOps Automation - Ansible

This is an automation script that are done up using Ansible to automatically deploy LAMP stack web server to a Ubuntu Environment. 

The original source code that are deploy is based on https://github.com/kodekloudhub/learning-app-ecommerce as part of proof of concepts, credits to Mumshad Mannambeth.

## Features
- Create LAMP stack server based on (Apache2, MariaDB, PHP)
- Supporting dependency are installed via apt-get via ansible automation script
- Pulling and deploying of PHP web page via git using cmd

## Tech
Ansible

For more information on how to setup Ansible on your environment, please check over here: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

- [Ansible] - Automation tools to automatically setup and configure through infrastructure as code.
- [PHP] - Web application

## Installation
- Pull the source code (Main)
- Make changes on inventory.txt file to update on:
  - host ip address
  - path to private key to access your ubuntu server
  - git_url (this is where you pull your source code)

## Verification
Verify the deployment by navigating to your host ip address in your preferred browser.

## Credit
The original source code that are used for deployment came from https://github.com/kodekloudhub/learning-app-ecommerce
